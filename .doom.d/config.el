;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
;;(setq user-full-name "John Doe"
;;      user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))
;;

;; Doom bindings example
;;(map! :leader
;;      (:prefix-map ("a" . "applications")
;;       (:prefix ("j" . "journal")
;;        :desc "New journal entry" "j" #'org-journal-new-entry
;;        :desc "Search journal entry" "s" #'org-journal-search)))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.

;; General stuff
(setq-default indent-tabs-mode nil)
; (setq-default tab-width 4) ; Assuming you want your tabs to be four spaces wide
; (defvaralias 'c-basic-offset 'tab-width)
(setq org-directory "~/org/")
(setq doom-theme 'doom-nord)
(setq doom-modeline-enable-word-count nil) ; speeds up scrolling
(setq display-line-numbers-type 'nil) ; speeds up scrolling

;; Merlin/OCaml settings
; Make company aware of merlin
(with-eval-after-load 'company
 (add-to-list 'company-backends 'merlin-company-backend))
(add-hook 'merlin-mode-hook 'company-mode) ; Enable company on merlin managed buffers
(setq company-dabbrev-downcase 0)
(setq company-idle-delay 0)

;; Eglot settings -- make it even more non-intrusive
(setq eglot-ignored-server-capabilites '(:documentHighlightProvider))
(setq eglot-ignored-server-capabilites '(:hoverProvider))

;; Keybindings
(global-set-key (kbd "C-j")
  (lambda ()
    (interactive)
    (forward-line 10)))

(global-set-key (kbd "C-k")
  (lambda ()
    (interactive)
    (forward-line -10)))

(setq-default c-basic-offset 4)

(evil-define-key nil evil-normal-state-map
  ";" 'evil-ex
  ":" 'evil-snipe-repeat)

;; Get out of brackets: copy/paste from reddit
(defun eide-smart-tab-jump-out-or-indent (&optional arg)
  "Smart tab behavior. Jump out quote or brackets, or indent."
  (interactive "P")
  (if (-contains? (list "\"" "'" ")" "}" ";" "|" ">" "]" ) (make-string 1 (char-after)))
      (forward-char 1)
    (indent-for-tab-command arg)))

(global-set-key (kbd "TAB")
  (lambda ()
    (interactive)
    (eide-smart-tab-jump-out-or-indent)))

