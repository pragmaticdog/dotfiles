"Plug-ins 
call plug#begin()
" Tools
    Plug 'ctrlpvim/ctrlp.vim'
    Plug 'jiangmiao/auto-pairs'
    Plug 'igankevich/mesonic'
    Plug 'preservim/nerdtree'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
" Syntax
    Plug 'tpope/vim-markdown'
    Plug 'ap/vim-css-color' "Displays a preview of colors with CSS 
" Color-schemes
    Plug 'arcticicestudio/nord-vim'
    Plug 'cocopon/iceberg.vim'
    Plug 'morhetz/gruvbox' "My favorite theme
    Plug 'kristijanhusak/vim-hybrid-material'
    Plug 'NLKNguyen/papercolor-theme'
    Plug 'ajh17/Spacegray.vim'
    Plug 'chriskempson/base16-vim'
call plug#end() 
 
"General Settings

"""""""" PROJECT SPECIFIC
nnoremap <silent> <space>m :!./run.sh<CR> 

set encoding=UTF-8
filetype plugin indent on  "Enabling Plugin & Indent
syntax on  "Turning Syntax on
set wildmenu
set autoread wildmode=longest:full,full
"set spell spelllang=en_us
set backspace=indent,eol,start confirm
set shiftwidth=4 autoindent smartindent tabstop=4 softtabstop=4 expandtab  
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
set hls is ic
set cmdheight=1
au BufRead,BufNewFile *.fountain set filetype=fountain
set splitbelow splitright 
set nobackup nowritebackup
packadd termdebug
" some nerdtree stuff
let NERDTreeQuitOnOpen = 1

"" tabline customisation
" Rename tabs to show tab number: from SO
if exists("+showtabline")
    function! MyTabLine()
        let s = ''
        let wn = ''
        let t = tabpagenr()
        let i = 1
        while i <= tabpagenr('$')
            let buflist = tabpagebuflist(i)
            let winnr = tabpagewinnr(i)
            let s .= '%' . i . 'T'
            let s .= (i == t ? '%1*' : '%2*')
            let s .= ' '
            let wn = tabpagewinnr(i,'$')

            let s .= '%#TabNum#'
            let s .= i
            let s .= (i == t ? '%#TabLineSel#' : '%#TabLine#')
            let bufnr = buflist[winnr - 1]
            let file = bufname(bufnr)
            let buftype = getbufvar(bufnr, 'buftype')
            if buftype == 'nofile'
                if file =~ '\/.'
                    let file = substitute(file, '.*\/\ze.', '', '')
                endif
            else
                let file = fnamemodify(file, ':p:t')
            endif
            if file == ''
                let file = '[No Name]'
            endif
            let s .= ' ' . file . ' '
            let i = i + 1
        endwhile
        let s .= '%T%#TabLineFill#%='
        let s .= (tabpagenr('$') > 1 ? '%999XX' : 'X')
        return s
    endfunction
    set stal=2
    set tabline=%!MyTabLine()
    set showtabline=1
    highlight link TabNum Special
endif

"""""" colorscheme / looks / airline
"let g:airline#extensions#tabline#tab_nr_type = 0 " # of splits (default)
let g:airline#extensions#tabline#tab_nr_type = 1 " tab number
"let g:airline#extensions#tabline#tab_nr_type = 2 " splits and tab number
let g:airline#extensions#tabline#tabnr_formatter = 'tabnr'

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#coc#enabled = 1

let g:airline_powerline_fonts = 1
colorscheme nord
set background=dark cursorline termguicolors
let g:airline_theme='nord'
let $FZF_DEFAULT_OPTS="--ansi --preview-window 'right:60%' --layout reverse --margin=1,4 --preview bat --color=always --style=header,grid --line-range :300 {}'"
hi! Normal ctermbg=NONE guibg=NONE 
hi! NonText ctermbg=NONE guibg=NONE guifg=NONE ctermfg=NONE 

" ALE
" vim-go
let g:ale_go_bingo_executable = 'gopls'
let g:ale_linters = { 'go': ['gopls'], }

let g:ale_sign_highlight_linenrs = 1
let g:ale_linters = {}
" let g:ale_sign_error = ""
" let g:ale_sign_warning = ""
let g:airline#extensions#ale#enabled = 1
let g:ale_fix_on_save = 1
let g:ale_set_highlights = 0
let g:ale_lint_on_text_changed = 'always'
let g:ale_sign_column_always = 1
let g:ale_echo_cursor = 1
nmap <F5> <Plug>(ale_fix)

"""""" Key-bindings
let mapleader=" "

" Go to tab by number
noremap <leader>1 1gt
noremap <leader>2 2gt
noremap <leader>3 3gt
noremap <leader>4 4gt
noremap <leader>5 5gt
noremap <leader>6 6gt
noremap <leader>7 7gt
noremap <leader>8 8gt
noremap <leader>9 9gt
noremap <leader>0 :tablast<cr>

" Open and close folds with space
nnoremap <Space> za
vnoremap <Space> za

" Map <Leader> x to close current buffer
nmap <leader>x :bd<CR>

" go to previous buffer
nnoremap <c-p> :e#<CR>

" trim whitespace
nnoremap <leader>rws :%s:\s\+$::<CR>:let @/=''<cr>

" I may try jk instead at some point.
" imap jj <Esc>

" remap pageup and pagedown -- easier on hands than c-f and c-b
nnoremap <silent> <c-n> <c-f>
nnoremap <silent> <c-m> <c-b>
" another handy remap
nnoremap <c-j> 10j
nnoremap <c-k> 10k

" change my mind: H and L are useless
" NOTE: H L to switch tabs
nnoremap H gT
nnoremap L gt

" go to last active tab
au TabLeave * let g:lasttab = tabpagenr()
nnoremap <silent> <c-l> :exe "tabn ".g:lasttab<cr>
vnoremap <silent> <c-l> :exe "tabn ".g:lasttab<cr>

nnoremap <leader>n :NERDTreeToggle<CR> 

" NOW, my hand doesn't hurt
" nnoremap \ ;
nnoremap ; :
nnoremap ' `

" really useful
noremap <leader>/ :noh<CR>

" nnoremap <leader>,  :split ~/AppData/Local/nvim/init.vim<CR> 
" nnoremap <leader>,  :e ~/.config/nvim/init.vim<CR> 
" nnoremap <C-s> :source ~/.config/nvim/init.vim<CR>

nnoremap <Up> :resize -2<CR> 
nnoremap <Down> :resize +2<CR>
nnoremap <Left> :vertical resize +2<CR>
nnoremap <Right> :vertical resize -2<CR>

" TODO: find a better short cut
nnoremap <leader><Down> :split<CR>
nnoremap <leader><Up> :split<CR>
nnoremap <leader><Left> :vsplit<cr>
nnoremap <leader><Right> :vsplit<cr>

" wtf is this?
xnoremap K :move '<-2<CR>gv-gv
xnoremap J :move '>+1<CR>gv-gv

nnoremap <leader>h <C-W>h
nnoremap <leader>j <C-W>j
nnoremap <leader>k <C-W>k
nnoremap <leader>l <C-W>l

